import React, { useState,useEffect } from "react";
import styles from "../styles/Account.module.css";
import Header from "../components/Header.js";
import LogoutIcon from "@mui/icons-material/Logout";
import RemoveShoppingCartIcon from '@mui/icons-material/RemoveShoppingCart';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

import { REGISTER_CUSTOMER, GET_USER } from "./api/requests.js";
import { useLazyQuery, useMutation } from "@apollo/client";
import { message } from "antd";
import { Tabs } from "antd";
import { useRouter } from "next/router";

export default function Account() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [email2, setEmail2] = useState("");
  const [password2, setPassword2] = useState("");
  const { TabPane } = Tabs;

  const [_getUsers] = useLazyQuery(GET_USER);
  const [_registerUser] = useMutation(REGISTER_CUSTOMER);

  let userId = null;

  if (typeof window !== "undefined") {
    userId = window.localStorage.getItem("userId");
  }

  const handleUserCreation = (e) => {
    e.preventDefault();

    const payload = {
      email,
      password,
      name,
    };

    _getUsers({
      variables: {
        email,
      },
    })
      .then((res) => {
        console.log(res.data.getCustomer === null ? "No user" : "Yes user");
        if (res.data.getCustomer !== null) {
          message.info(
            "We found an account with this email. If it is yours, please sign in"
          );
        } else {
          _registerUser({
            variables: payload,
          })
            .then((res) => {
              console.log(res.data);
              message.success("Success! Profile created! ");
              try {
                window.localStorage.setItem("userId", res.data.addCustomer.id);
              } catch (err) {
                console.log(err);
              }
            })
            .catch((err) => console.log(err.message));
        }
      })
      .catch((err) => console.log(err));
  };

  const handleSignIn = () => {
    _getUsers({
      variables: {
        email: email2,
        password: password2,
      },
    })
      .then((res) => {
        console.log(res.data.getCustomer === null ? "No user" : "Yes useer");
        if (res.data.getCustomer === null) {
          message.info("no account found.please register");
        } else {
          try {
            window.localStorage.setItem("userId", res.data.getCustomer.id);
            window.location = "/account";
          } catch (err) {
            console.log(err);
          }
        }
      })
      .catch((err) => console.log(err));
  };

  function callback(key) {
    console.log(key);
  }

  console.log(userId);

  return (
    <div>
      <Header />
      {userId != null ? (
        <AccountData />
      ) : (
        <Tabs
          defaultActiveKey="1"
          onChange={callback}
          className={styles.container}
        >
          <TabPane tab="Register" key="1">
            <form className={styles.form}>
              <input
                type="name"
                placeholder="Name"
                className={styles.input}
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
              <input
                type="email"
                placeholder="Email"
                className={styles.input}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
              <input
                type="password"
                placeholder="Password"
                className={styles.input}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
              <div className={styles.checkbox_container}>
                <input type="checkbox" required />
                <span>
                  I agree to the{" "}
                  <a href="" className={styles.a}>
                    Terms&Conditions
                  </a>
                  .
                  <br />
                  <a href="" className={styles.a}>
                    Privacy Policy
                  </a>{" "}
                  of Try fashion
                </span>
              </div>
              <button
                className={styles.submit_btn}
                onClick={handleUserCreation}
                type='submit'
              >
                Create Account
              </button>
            </form>
          </TabPane>
          <TabPane tab="Sign in" key="2">
            <form className={styles.form}>
              <input
                type="email"
                value={email2}
                onChange={(e) => setEmail2(e.target.value)}
                placeholder="Email"
                className={styles.input}
                required
              />
              <input
                type="password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                placeholder="Password"
                className={styles.input}
                required
              />
              <div>
                <button
                  className={styles.submit_btn}
                  type="submit"
                  onClick={(e) => {
                    e.preventDefault();
                    handleSignIn();
                  }}
                >
                  Sign In
                </button>
                <span
                  style={{
                    color: "red",
                    fontWeight: "bold",
                    fontSize: "12px",
                    float: "right",
                  }}
                >
                  Forgot password ?
                </span>
              </div>
            </form>
          </TabPane>
        </Tabs>
      )}
    </div>
  );
}

const AccountData = () => {
  const { TabPane } = Tabs;
  const router = useRouter();
  // Must have sign out option

  const signOut = () => {
    if (typeof window !== "undefined") {
      try {
        window.localStorage.clear();
        router.reload();
      } catch (err) {
        console.log("Error signing out");
      }
    }
  };

  const [email,setEmail]=useState('')
  const [username,setUserName]=useState('')
  const [_getUsers] = useLazyQuery(GET_USER);
  const getDetails=()=>{
      _getUsers()
      .then((res)=>{
        console.log(res.data.getCustomer.name);
        setEmail(res.data.getCustomer.email);
        setUserName(res.data.getCustomer.name);

      })
      .catch((err)=>{console.log('failed')})
  }

  useEffect(()=>{
    getDetails();
  },[])
  return (
    <Tabs style={{padding:"20px"}}>
        <TabPane tab="Purchases" key="1" style={{padding:"10px",lineHeight:"30px"}}>
          <RemoveShoppingCartIcon />
          <p>No orders have been found...</p>
          <button
            onClick={()=>{router.push("/filter")}}
            style={{
              padding: "10px",
              border: "none",
              alignItems: "center",
              display: "flex",
              marginTop: "30px",
              cursor: "pointer",
            }}
          >
            <p>Go to Online Store</p>
            <ArrowForwardIcon style={{ fontSize: "0.9rem", margin: "0 10px" }} />
          </button>
        </TabPane>
        <TabPane tab="Profile" key="2" onClick={getDetails}>
        <p style={{fontSize:'1.2rem',fontWeight:'bold'}}>{username}</p>
        <p style={{fontSize:'1rem'}}>{email}</p>
        
          <button
            onClick={signOut}
            style={{
              padding: "10px",
              border: "none",
              alignItems: "center",
              display: "flex",
              marginTop: "30px",
              cursor: "pointer",
            }}
          >
            <p>Sign out</p>{" "}
            <LogoutIcon style={{ fontSize: "0.9rem", margin: "0 10px" }} />
          </button>
        </TabPane>
        <TabPane tab="Settings" key="3">
          <p>settings</p>
        </TabPane>
      </Tabs>
  );
};
