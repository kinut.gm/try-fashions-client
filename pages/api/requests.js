import { gql } from "@apollo/client";

// Params : subcategory
const GET_PRODUCT_COUNT = gql`
  query GET_PRODUCT_COUNT($subCategory: String) {
    getProductCount(subCategory: $subCategory)
  }
`;

const GET_CATEGORIES = gql`
  query GET_CATEGORIES {
    getCategories {
      id
      name
    }
  }
`;

const SET_PAID_TRUE = gql`
  mutation SET_PAID_TRUE($id:ID!) {
    setPaidTrue(id:$id){
      id
      paid
    }
  }
`

// Params : category , subCategory , lastest , recommended
const GET_PRODUCTS = gql`
  query GET_PRODUCTS(
    $category: String
    $subCategory: String
    $limit: Int
    $index: Int
    $latest: Boolean
    $recommended: Boolean
  ) {
    getProducts(
      category: $category
      subCategory: $subCategory
      limit: $limit
      index: $index
      latest: $latest
      recommended: $recommended
    ) {
      id
      name
      category
      subCategory
      image1
      price
      previousPrice
      showPreviousPrice
    }
  }
`;

// Params : id
const GET_PRODUCT = gql`
  query GET_PRODUCT($id: ID!) {
    getProduct(id: $id) {
      name
      price
      colors
      image1
      image2
      image3
      image4
      sizes
      previousPrice
      showPreviousPrice
      description
      reviews {
        message
        id
        customer {
          name
        }
        createdAt
      }
      suggestions {
        name
        id
        image1
        price
      }
    }
  }
`;

const ADD_REVIEW = gql`
  mutation ADD_REVIEW($message: String, $customer: ID!, $product: ID!) {
    addReview(message: $message, customer: $customer, product: $product) {
      id
      message
    }
  }
`;

const ADD_ORDER = gql`
  mutation ADD_ORDER(
    $product: ID!
    $customer: ID!
    $size: String
    $quantity: Int
  ) {
    addOrder(
      product: $product
      customer: $customer
      size: $size
      quantity: $quantity
    ) {
      id
    }
  }
`;

// Subscription on Customer model
const GET_USER = gql`
  query GET_USER($id: ID, $name: String, $email: String, $password: String) {
    getCustomer(id: $id, name: $name, email: $email, password: $password) {
      id
      name
      email
      phoneNumber
      password
      photoUrl
      recentSearches
      basket {
        id
        product {
          name
          price
          image1
          showPreviousPrice
          previousPrice
        }
        size
        quantity
      }
      oldOrders {
        id
        product {
          name
          price
          showPreviousPrice
          previousPrice
        }
        size
        quantity
      }
    }
  }
`;

const INCREMENT_ORDER = gql`
  mutation INCREMENT_ORDER($id: ID!) {
    incrementorderCount(id: $id) {
      id
      quantity
    }
  }
`;

const DECREMENT_ORDER = gql`
  mutation DECREMENT_ORDER($id: ID!) {
    decrementOrderCount(id: $id) {
      id
      quantity
    }
  }
`;

const REMOVE_FROM_BASKET = gql`
  mutation REMOVE_FROM_BASKET($id: ID!) {
    removeFromCart(id: $id) {
      id
    }
  }
`;

const ADD_TRANSACTION = gql`
  mutation ADD_TRANSACTION(
    $tx_ref: String
    $transaction_id: String
    $orders: [ID!]!
    $amount: Int
  ) {
    addTransaction(
      tx_ref: $tx_ref
      transaction_id: $transaction_id
      orders: $orders
      amount: $amount
    ) {
      tx_ref
      transaction_id
      orders
      amount
    }
  }
`;

const REGISTER_CUSTOMER = gql`
  mutation REGISTER_CUSTOMER(
    $name: String!
    $email: String
    $password: String
    $photoUrl: String
    $phoneNumber: String
  ) {
    addCustomer(
      name: $name
      email: $email
      password: $password
      photoUrl: $photoUrl
      phoneNumber: $phoneNumber
    ) {
      id
      name
      email
    }
  }
`;

const UPDATE_ORDER = gql`
  mutation UPDATE_ORDER($id: ID!, $paid: Boolean) {
    updateOrder(id: $id, paid: $paid) {
      id
      paid
      deliveryDate
      cancelled
      customer {
        name
      }
    }
  }
`;

export {
  REGISTER_CUSTOMER,
  REMOVE_FROM_BASKET,
  DECREMENT_ORDER,
  INCREMENT_ORDER,
  GET_USER,
  ADD_ORDER,
  ADD_REVIEW,
  GET_PRODUCT,
  GET_PRODUCTS,
  SET_PAID_TRUE,
  GET_PRODUCT_COUNT,
  GET_CATEGORIES,
  ADD_TRANSACTION,
  UPDATE_ORDER,
};
