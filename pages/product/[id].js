/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Carousel, Input, Modal } from "antd";
import styles from "../../styles/Product.module.css";
import { Select } from "antd";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import ShoppingBagIcon from "@mui/icons-material/ShoppingBag";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import GradeIcon from "@mui/icons-material/Grade";
import Header from "../../components/Header.js";
import { SpinnerRoundFilled } from "spinners-react";

import { useRouter } from "next/router";
import { useMutation, useQuery } from "@apollo/client";
import { GET_PRODUCT, ADD_REVIEW, ADD_ORDER } from "../api/requests";

const { Option } = Select;

export default function Product() {
  const router = useRouter();

  const { id } = router.query;

  let user;

  if (typeof window !== "undefined") {
    user = localStorage.getItem("userId");
  }

  const {
    data: productData,
    loading: productLoading,
    error: productError,
  } = useQuery(GET_PRODUCT, {
    variables: {
      id,
    },
  });

  const [_addReview] = useMutation(ADD_REVIEW);
  const [_addOrder] = useMutation(ADD_ORDER);

  const [counter, setCounter] = useState(1);
  const [modalVisible, setModalVisible] = useState(false);
  const [review, setReview] = useState("");
  const [selectedSize, setSelectedSize] = useState("");
  const [quantity, setQuantity] = useState(1);

  if (productLoading || id == (null || undefined))
    return (
      <div
        style={{
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          margin:"25vh 0"
        }}
      >
        <SpinnerRoundFilled size={100} speed={50} color={"#d2b48c"} />
        <p
          style={{
            marginTop: "10",
            fontWeight: "600",
            fontSize: "2rem",
            display: "flex",
          }}
        >
          Try<p style={{ margin: "0", color: "#d2b48c" }}>Fashion</p>
        </p>
      </div>
    );
  if (productError) return <p>Error fetching product info</p>;

  const addToCart = () => {
    if (productData.getProduct.sizes.length > 0 && selectedSize == "") {
      alert("Please pick a size in the options");
    } else if (user == null) {
      alert("Please sign in to add to cart");
    } else {
      _addOrder({
        variables: {
          product: id,
          customer: user,
          size: selectedSize,
          quantity,
        },
      })
        .then((res) => {
          alert("Product successfully added to cart");
          setQuantity(1);
          setSelectedSize("");
        })
        .catch((err) => {
          alert(err.message);
        });
    }
  };

  const submitReview = () => {
    if (user == null) {
      alert("Please log in to send a review");
    } else {
      _addReview({
        variables: {
          product: id,
          message: review,
          customer: user,
        },
      })
        .then(() => alert("Review added. Refresh to see changes"))
        .catch((err) => alert(err.message));
    }
  };

  return (
    <div className={styles.body}>
      <Header />
      <div className={styles.Topbody}>
        <div className={styles.section_1}>
          <Carousel autoplay className={styles.image_carousel}>
            <img
              src={productData.getProduct.image1}
              alt="logo"
              className={styles.image_container}
            />
            <img
              src={productData.getProduct.image2}
              alt="logo"
              className={styles.image_container}
            />
            <img
              src={productData.getProduct.image3}
              alt="logo"
              className={styles.image_container}
            />
            <img
              src={productData.getProduct.image4}
              alt="logo"
              className={styles.image_container}
            />
          </Carousel>
        </div>
        <div className={styles.section_2}>
          <h3 className={styles.product_name}>{productData.getProduct.name}</h3>
          <p className={styles.product_price}>
            Ksh {productData.getProduct.price}
          </p>
          {productData.getProduct.showPreviousPrice && (
            <>
              <span className={styles.product_discount}>
                ksh{" "}
                {productData.getProduct.previousPrice == null
                  ? "N/A"
                  : productData.getProduct.previousPrice}
              </span>
              <span className={styles.product_discount_tag}>
                -
                {productData.getProduct.previousPrice == null
                  ? 0
                  : (
                      ((productData.getProduct.previousPrice -
                        productData.getProduct.price) /
                        productData.getProduct.price) *
                      100
                    ).toFixed(2)}
                %
              </span>
            </>
          )}

          <div className={styles.product_size_conatiner}>
            <h1 className={styles.product_size_title}>Sizes</h1>
            <div className={styles.product_size_button_container}>
              {productData.getProduct.sizes.map((size) => (
                <button
                  key={size}
                  onClick={() => setSelectedSize(size)}
                  style={{ backgroundColor: selectedSize == size && "brown" }}
                  className={styles.product_size_button}
                >
                  {size}
                </button>
              ))}
            </div>
          </div>
          <div>
            <h1
              style={{
                fontSize: "0.9rem",
                fontWeight: "bold",
                color: "#d2b48c",
              }}
            >
              Description
            </h1>
            <p>
              Color :{" "}
              {productData.getProduct.colors.map((color) => (
                <span key={color}>{color},</span>
              ))}
            </p>
          </div>
          <div
            style={{
              display: "flex",
              fontSize: "0.6rem",
              alignItems: "center",
              marginTop: "5px",
            }}
          >
            <LocalShippingIcon
              style={{ fontSize: "1.2rem", marginRight: "10px" }}
            />
            <div>
              <p>Fast Delivery</p>
              <p>In stock, usually dispatched in 1 business days.</p>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              fontSize: "0.6rem",
              alignItems: "center",
              marginTop: "5px",
            }}
          >
            <AccessTimeIcon
              style={{ fontSize: "1.2rem", marginRight: "10px" }}
            />
            <p>30-day Return Policy</p>
          </div>
          <div>
            <h1
              style={{
                fontSize: "0.9rem",
                fontWeight: "bold",
                color: "#d2b48c",
              }}
            >
              Overview
            </h1>
            <p>
              {productData.getProduct.description == null
                ? "No description"
                : productData.getProduct.description}
            </p>
          </div>
          <div className={styles.add_to_cart_container}>
            <div className={styles.value_container}>
              <Select value={quantity} onChange={(val) => setQuantity(val)}>
                {[1, 2, 3, 4, 5, 6, 7].map((el) => (
                  <Option value={el} key={el}>
                    {el}
                  </Option>
                ))}
              </Select>
            </div>
            <button className={styles.add_to_cart_button} onClick={addToCart}>
              <ShoppingBagIcon style={{ fontSize: "16px", margin: "0 5px" }} />
              Add to cart
            </button>
          </div>
        </div>
      </div>
      <div style={{ backgroundColor: "#e5e5e5", padding: "10px" }}>
        <h1 style={{ fontSize: "0.9rem", fontWeight: "bold" }}>
          Customers reviews ({productData.getProduct.reviews.length})
        </h1>

        {productData.getProduct.reviews.slice(0, 3).map((review) => (
          <div
            key={review.id}
            style={{
              backgroundColor: "#fff",
              padding: "10px",
              borderRadius: "5px",
            }}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <AccountCircleIcon style={{ fontsize: "24px" }} />
              <div>
                <span style={{ fontSize: "12px", fontWeight: "700" }}>
                  {review.customer.name}
                </span>
                <div style={{ display: "flex" }}>
                  <GradeIcon style={{ fontSize: "14px", color: "#d2b48c" }} />
                  <GradeIcon style={{ fontSize: "14px", color: "#d2b48c" }} />
                  <GradeIcon style={{ fontSize: "14px", color: "#d2b48c" }} />
                  <GradeIcon style={{ fontSize: "14px", color: "#d2b48c" }} />
                  <GradeIcon style={{ fontSize: "14px", color: "#d2b48c" }} />
                </div>
              </div>
            </div>
            <div>
              <p style={{ margin: "0 0" }}>{review.message}</p>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  marginTop: "5px",
                  color: "grey",
                }}
              >
                <AccessTimeIcon
                  style={{ fontSize: "1.2rem", marginRight: "10px" }}
                />
                <p style={{ margin: "0 0" }}>
                  {new Date(parseInt(review.createdAt)).toDateString()}
                </p>
              </div>
            </div>
          </div>
        ))}

        <button
          onClick={() => setModalVisible(true)}
          style={{
            color: "#000",
            backgroundColor: "#fff",
            border: "none",
            borderRadius: "3px",
            fontWeight: "bold",
            marginTop: "10px",
            padding: "10px",
          }}
        >
          Add Review
        </button>

        <Modal
          visible={modalVisible}
          onOk={submitReview}
          okText="Submit"
          onCancel={() => {
            setModalVisible(false);
            setReview("");
          }}
        >
          <h3>Add review</h3>
          <Input
            value={review}
            onChange={(e) => {
              setReview(e.target.value);
            }}
            placeholder="Write your review here"
          />
        </Modal>
      </div>
      <div>
        <h1
          style={{
            fontSize: "0.9rem",
            fontWeight: "bold",
          }}
        >
          You may also like
        </h1>
        <div style={{ display: "flex", overflowX: "scroll" }}>
          {productData.getProduct.suggestions.map((product) => (
            <Item product={product} key={product.id}/>
          ))}
        </div>
      </div>
    </div>
  );
}

const Item = ({ product }) => {
  const router = useRouter();

  return (
    <div
      onClick={() => {
        router.push(`/product/${product.id}`);
      }}
      style={{
        width: "150px",
        height: "",
        fontSize: "12px",
        margin: "10px",
        position: "relative",
      }}
    >
      <img
        src={product.image1}
        style={{ width: "100%", height: "150px", objectFit: "cover" }}
        alt="image"
      />
      <p style={{ fontWeight: "600", color: "grey" }}>{product.name}</p>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <p style={{ fontWeight: "600" }}>Ksh. {product.price}</p>
      </div>
    </div>
  );
};
