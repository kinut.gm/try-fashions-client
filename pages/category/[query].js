import { useQuery } from "@apollo/client";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { GET_PRODUCTS } from "../api/requests.js";
import Header from "../../components/Header.js";
import styles from "../../styles/Home.module.css";
import { SpinnerRoundFilled } from "spinners-react";

export default function Category() {
  const router = useRouter();

  const [args, setArgs] = useState(null);
  const [keyword, setKeyword] = useState("");

  let _args = {};
  let { query } = router.query; 
  console.log(router.asPath)

  //console.log(query);

  switch (query) {
    case "new in":
      _args.latest = true;
      break;
    case "all clothing":
      _args = {};
      break;
    case "men":
      _args.category = "men";
      break;

    case "ladies":
      _args.category = "ladies";
      break;
    case "kids":
      _args.category = "kids";
      break;
    
    default:
      _args.subCategory = query;
      break;
  }

  const {
    data: pData,
    loading: pLoading,
    error: pError,
  } = useQuery(GET_PRODUCTS, {
    variables: _args,
  });

  const getQuery = (_keyword) => {
    setKeyword(_keyword);
  };

  if (pError) return <p>Error loading products</p>;
  if (pLoading)
    return (
      <div
        style={{
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          margin:"25vh 0"
        }}
      >
        <SpinnerRoundFilled size={100} speed={50} color={"#d2b48c"} />
        <p
          style={{
            marginTop: "10",
            fontWeight: "600",
            fontSize: "2rem",
            display: "flex",
          }}
        >
          Try<p style={{ margin: "0", color: "#d2b48c" }}>Fashion</p>
        </p>
      </div>
    );
  //console.log(args);

  return (
    <div>
      <Header getQuery={getQuery} />

      <div
        style={{
          margin: "10px 10px",
          display: "flex",
          alignItems: "baseline",
          backgroundColor: "#d2b48c",
          padding: "10px",
          width: "100%",
        }}
      >
        <span
          style={{ fontWeight: "800px", fontSize: "1rem", fontWeight: "bold" }}
        >
          {router.query.query.charAt(0).toUpperCase() +
            router.query.query.slice(1, router.query.query.length)}
        </span>
        <span
          style={{
            fontSize: "12px",
            fontWeight: "bold",
            padding: "0 10px",
            color: "grey",
          }}
        >
          {
            pData.getProducts.filter((product) =>
              product.name.toLowerCase().includes(keyword.toLowerCase())
            ).length
          }{" "}
          products
        </span>
      </div>

      <div className={styles.body}>
        <div className={styles.product_section}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              flexWrap: "wrap",
              marginTop: "10px",
            }}
          >
            {pData.getProducts.length > 0 ? (
              pData.getProducts
                .filter((product) =>
                  product.name.toLowerCase().includes(keyword.toLowerCase())
                )
                .filter((product) =>
                  product.name.toLowerCase().includes(keyword.toLowerCase())
                )
                .map((product) => {
                  return <Product key={product.id} data={product} />;
                })
            ) : (
              <p>Category empty</p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

const Product = ({ data }) => {
  const router = useRouter();
  return (
    <div
      onClick={() => router.push(`/product/${data.id}`)}
      style={{
        width: "200px",
        fontSize: "12px",
        margin: "10px",
        position: "relative",
        cursor:'pointer',
        borderRadius:'5px',
        boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)'
      }}
    >
      <img
        onClick={() => {
          router.push(`/product/${data.id}`);
        }}
        src={data.image1}
        style={{ width: "200px", height: "200px",objectFit:'cover' }}
        alt="image"
      />
      <div style={{padding:10,}}>
        <p style={{ fontWeight: "600", color: "grey", overflow: "hidden" }}>
          {data.name}
        </p>
        <p style={{ fontWeight: "600" }}>Ksh. {data.price}</p>
      </div>
    </div>
  );
};
