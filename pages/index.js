/* eslint-disable @next/next/no-img-element */
import { useState } from "react";
import styles from "../styles/Home.module.css";
import "antd/dist/antd.css";
import { Carousel } from "antd";
import { useRouter } from "next/router";
import Header from "../components/Header.js";
import { data } from "../components/Data.js";
import { SpinnerRoundFilled } from "spinners-react";

import Footer from "../components/Footer.js";
import StorefrontIcon from "@mui/icons-material/Storefront";
import SupportAgentIcon from "@mui/icons-material/SupportAgent";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import CreditScoreIcon from "@mui/icons-material/CreditScore";

import { GET_PRODUCTS } from "./api/requests.js";
import { useQuery } from "@apollo/client";

export default function Home() {
  const router = useRouter();

  const [keyword, setKeyword] = useState("");

  const {
    data: newData,
    loading: newLoading,
    error: newError,
  } = useQuery(GET_PRODUCTS, {
    variables: {
      latest: true,
      limit: 10,
      index: 0,
    },
  });

  console.log(newData);

  if (newLoading)
    return (
      <div
        style={{
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          margin:"25vh 0"
        }}
      >
        <SpinnerRoundFilled size={100} speed={50} color={"#d2b48c"} />
        <p
          style={{
            marginTop: "10",
            fontWeight: "600",
            fontSize: "2rem",
            display: "flex",
          }}
        >
          Try<p style={{ margin: "0", color: "#d2b48c" }}>Fashion</p>
        </p>
      </div>
    );
  if (newError) return <p>Error new-in data</p>;
  // if (recommendedLoading) return <p>Loading recommended data</p>;
  // if (recommendedError) return <p>Error recommended data</p>;

  return (
    <div className={styles.container}>
      <Header
        style={{ marginTop: "40px", backgroundColor: "red" }}
        getQuery={(val) => router.push(`/filter/${val}`)}
      />
      <Carousel autoplay className={styles.image_carousel}>
        <div style={{ position: "relative" }}>
          <img
            src="IMG_7406_540x.webp"
            alt="logo"
            className={styles.image_container}
          />
          <div
            className={styles.fadeInUp}
            style={{
              position: "absolute",
              top: "150px",
              width: "200px",
              float: "right",
              zIndex: "1",
              textAlign: "center",
            }}
          >
            <p style={{ fontSize: "1.2rem", color: "#fff" }}>That snug fit</p>
            <p style={{ fontSize: "1.6rem", color: "#fff", fontWeight: "800" }}>
              Fits like a glove
            </p>
            <button
              onClick={() => {
                router.push("filter/");
              }}
              className={styles.carouselbutton}
            >
              Shop Now
            </button>
          </div>
        </div>
      </Carousel>
      <div className={styles.new_arrivals}>
        <h2
          style={{
            marginRight: "10px",
            alignItems: "center",
            textAlign: "left",
            borderBottom: "2px solid grey",
            width: "150px",
            color: "#d2b48c",
            fontWeight: "700",
          }}
        >
          NEW IN
        </h2>
        <div
          style={{
            display: "flex",
            overflow: "auto",
            whiteSpace: "nowrap",
            width: "100%",
            margin: "0",
          }}
          className={styles.scrollbar}
        >
          {newData.getProducts
            .filter((product) =>
              product.name.toLowerCase().includes(keyword.toLowerCase())
            )
            .map((data) => {
              return <Product key={data.id} data={data} />;
            })}
        </div>
      </div>
      <div className={styles.categories}>
        <h2
          style={{
            marginRight: "10px",
            alignItems: "center",
            textAlign: "center",
            borderBottom: "2px solid grey",
            width: "150px",
            color: "#d2b48c",
            fontWeight: "700",
          }}
        >
          COLLECTIONS
        </h2>
        <div className={styles.category_container}>
          <div className={styles.category_subcontainer1}>
            <div style={{ position: "relative",boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)' }}>
              <img
                onClick={() =>
                  (window.location = `/category/${'ladies'.toLowerCase()}`)
                }
                src="image10.jpg"
                alt="logo"
                className={styles.category_image_1}
              />
              <span
                style={{
                  position: "absolute",
                  top: "10px",
                  left: "10px",
                  padding: "10px",
                  backgroundColor: "rgb(218, 216, 216, 0.3)",
                  fontWeight: "700",
                }}
              >
                Ladies
              </span>
            </div>
            <div className={styles.category_subcontainer_1}>
              <div style={{ position: "relative",boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)' }}>
                <img
                onClick={() =>
                  (window.location = `/category/${'Accessories'.toLowerCase()}`)
                }
                  src="image6.png"
                  alt="logo"
                  className={styles.category_image_2}
                />
                <span
                  style={{
                    position: "absolute",
                    top: "20px",
                    left: "20px",
                    padding: "10px",
                    backgroundColor: "rgb(218, 216, 216, 0.3)",
                    fontWeight: "700",
                  }}
                >
                  Accessories
                </span>
              </div>
              <div style={{ position: "relative", flexGrow: "1",boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)' }}>
                <img
                onClick={() =>
                  (window.location = `/category/${'New Arrivals'.toLowerCase()}`)
                }
                  src="image11.jpg"
                  alt="logo"
                  className={styles.category_image_3}
                />
                <span
                  style={{
                    position: "absolute",
                    top: "20px",
                    left: "20px",
                    padding: "10px",
                    backgroundColor: "rgb(218, 216, 216, 0.3)",
                    fontWeight: "700",
                  }}
                >
                  New Arrivals
                </span>
              </div>
            </div>
          </div>
          <div className={styles.category_subcontainer2}>
            <div style={{ position: "relative",boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)' }}>
              <div
              onClick={() =>
                (window.location = `/category/${'all clothing'.toLowerCase()}`)
              }
                className={styles.category_image_5}
              />
              <span
                style={{
                  position: "absolute",
                  top: "20px",
                  left: "20px",
                  padding: "10px",
                  backgroundColor: "rgb(218, 216, 216, 0.3)",
                  fontWeight: "700",
                }}
              >
                Discover
              </span>
            </div>
            <div style={{ position: "relative",boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)' }}>
            <img
            onClick={() =>
              (window.location = `/category/${'men'.toLowerCase()}`)
            }
                src="image8.jpg"
                alt="logo"
                className={styles.category_image_4}
              />
              <span
                style={{
                  position: "absolute",
                  top: "20px",
                  left: "20px",
                  padding: "10px",
                  backgroundColor: "rgb(218, 216, 216, 0.3)",
                  fontWeight: "700",
                }}
              >
                Men
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.new_arrivals}>
        <h3
          style={{
            marginRight: "10px",
            alignItems: "center",
            textAlign: "left",
            borderBottom: "2px solid grey",
            width: "200px",
            color: "#d2b48c",
            fontWeight: "700",
          }}
        >
          RECOMMENDED FOR YOU
        </h3>
        <div
          style={{
            display: "flex",
            overflow: "auto",
            whiteSpace: "nowrap",
            width: "100%",
            margin: "0",

          }}
          className={styles.scrollbar}
        >
          {newData.getProducts.map((data) => {
            return <Product key={data.id} data={data} />;
          })}
        </div>
        <button
          onClick={() => {
            router.push("/filter/");
          }}
          className={styles.quick_view}
        >
          View All
        </button>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          backgroundColor: "#e5e5e5",
          width: "90%",
          margin: "20px 5%",
          padding: "10px",
          textAlign: "center",
          fontSize: "0.8rem",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <StorefrontIcon />
          <div>
            <p style={{ fontWeight: "800" }}>High-quality Goods</p>
            <span>
              Enjoy top quality
              <br /> items for less
            </span>
          </div>
        </div>
        <div>
          <SupportAgentIcon />
          <div>
            <p style={{ fontWeight: "800" }}>24/7 Livechat</p>
            <span>
              Get instant assistance <br /> whenever you need it
            </span>
          </div>
        </div>
        <div>
          <LocalShippingIcon />
          <div>
            <p style={{ fontWeight: "800" }}>Express Shipping</p>
            <span>
              Fast & reliable <br />
              delivery options
            </span>
          </div>
        </div>
        <div>
          <CreditScoreIcon />
          <div>
            <p style={{ fontWeight: "800" }}>Secure Payment</p>
            <span>
              Multiple safe
              <br /> payment methods
            </span>
          </div>
        </div>
      </div>
      <div className={styles.location_conatiner}>
        <div className={styles.location_information}>
          <h2 style={{ color: "#fff" }}>
            YOU CAN NOW SHOP AND PICK UP YOUR CLOTHES AT BEBABEBA TRADE CENTRE
            STORE NO. A1-A2!
          </h2>
          <p>
            Check our Instagram and Facebook for more updates and more deals!
          </p>
        </div>
        <img src="city.jpg" alt="" className={styles.location_image} />
      </div>
      <Footer />
    </div>
  );
}

const Product = ({ data }) => {
  const router = useRouter();
  return (
    <div
      onClick={() => router.push(`/product/${data.id}`)}
      style={{
        width: "200px",
        fontSize: "12px",
        margin: "10px",
        position: "relative",
        cursor:'pointer',
        borderRadius:'5px',
        boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)'
      }}
    >
      <img
        onClick={() => {
          router.push(`/product/${data.id}`);
        }}
        src={data.image1}
        style={{ width: "200px", height: "250px" }}
        alt="image"
      />
      <div style={{padding:10,}}>
        <p style={{ fontWeight: "600", color: "grey", overflow: "hidden",fontSize:'0.9rem' }}>
          {data.name}
        </p>
        <p style={{ fontWeight: "600" }}>Ksh. {data.price}</p>
      </div>
    </div>
  );
};
