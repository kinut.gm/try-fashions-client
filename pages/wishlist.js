/* eslint-disable react/no-unescaped-entities */
import React from 'react'
import Header from '../components/Header.js'
import { useRouter } from 'next/router'

export default function Wishlist(){
 const router = useRouter()
 return(
  <div>
  <Header />
   <div style={{textAlign:"center",lineHeight:"40px",marginTop:"40px"}}>
    <p>You don't have any Items saved</p>
    <p>Log in to sync your Items</p>
    <button style={{padding:"10px",
       backgroundColor:"#000",
       color:"#fff",
       border:"none"
       }} onClick={()=>{router.push('account')}}>Log In</button>
   </div>
  </div>
  
 )
}