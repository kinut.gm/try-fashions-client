/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @next/next/no-img-element */
import React, { useLayoutEffect, useState,useRef } from "react";
import { useRouter } from "next/router";
import styles from "../styles/Cart.module.css";
import CartItem from "../components/CartItem.js";
import { useLazyQuery, useMutation } from "@apollo/client";
import { GET_USER, ADD_TRANSACTION, UPDATE_ORDER, SET_PAID_TRUE } from "./api/requests.js";
import Header from "../components/Header.js";
import { useFlutterwave, closePaymentModal } from "flutterwave-react-v3";
import { Tabs, Modal, Divider, message } from "antd";
import Crypto from '../components/crypto.js'
const { TabPane } = Tabs;

export default function Cart() {
  const [basket, setBasket] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [city, setcity] = useState("");
  const [address, setaddress] = useState("");
  const [phone, setphone] = useState("");

  const [_getUser] = useLazyQuery(GET_USER);
  const [_addTransaction] = useMutation(ADD_TRANSACTION);
  const [_updateOrder] = useMutation(UPDATE_ORDER);
  const [_setPaidTrue] = useMutation(SET_PAID_TRUE)

  const router = useRouter();
  useLayoutEffect(() => {
    let userId;

    if (typeof window !== "undefined") {
      userId = window.localStorage.getItem("userId");
      console.log(userId);

      try {
        _getUser({
          variables: {
            id: userId,
          },
        })
          .then((res) => {
            setBasket([]);
            setBasket([...basket, ...res.data.getCustomer.basket]);
            console.log(res.data)
          })
          .catch((err) => console.log(err.message));
      } catch (err) {
        console.log(err);
      }
      
    }
  },[]);
console.log(basket)
  const getTotal = () => {
    let total = 0;

    basket.forEach((cartItem) => {
      total = total + cartItem.product.price * cartItem.quantity;
    });

    return total;
  };

  const config = {
    public_key: "FLWPUBK-37bab4fd2cbbc27af95dc48a57ce834b-X",
    tx_ref: Date.now(),
    amount: getTotal(),
    currency: "KES",
    payment_options: "card,mobilemoney",
    customer: {
      email: email,
      phonenumber: phone,
      name: name,
    },
    customizations: {
      title: "TRY FASHION",
      description: "Payment for items in cart",
      logo: "https://st2.depositphotos.com/4403291/7418/v/450/depositphotos_74189661-stock-illustration-online-shop-log.jpg",
    },
  };

  const handleFlutterPayment = useFlutterwave(config);

  const handleCancelModal = () => {
    setModalVisible(false);
  };

  const handleSaveNPay = () => {
    let address = {
      address,
      city,
      name,
      email,
      phone,
    };
    
    console.log(address.email);

    let _orders = [];

    basket.forEach((order) => _orders.push(order.id));

    handleFlutterPayment({
      callback: (response) => {
        console.log(response);
        if (response.status == "successful") {
          _addTransaction({
            variables: {
              tx_ref: response.tx_ref.toString(),
              transaction_id: response.transaction_id.toString(),
              amount: response.amount,
              orders: _orders,
            },
          })
            .then(() => {
              _orders.forEach((order, index) => {
                console.log("Order id", order);
                _setPaidTrue({
                  variables: {
                    id: order.toString(),
                  }
                })            
                  .then((res) => console.log(res.data.updateOrder))
                  .then(() => message.success(`Cart item ${index} paid for`))
                  .then(() => router.reload());
              });
            })
            .catch((err) => message.error("Failed"));
        } else {
          message.error("Transaction failed");
        }
        closePaymentModal(); // this will close the modal programmatically
      },
      onClose: () => {},
    });
  };
  const Payprice = (getTotal() * 0.00962).toFixed(2); 
  console.log(Payprice)
  //Paypal Payments
  const paypal = useRef();

  const HandlePay=()=>{
    console.log(typeof(Payprice))
    let _orders = [];

    basket.forEach((order) => _orders.push(order.id));

    window.paypal
      .Buttons({
        style: {
          shape: 'rect',
          color: 'white',
          layout: 'horizontal',
          label: 'paypal',
        },
        createOrder: (data, actions, err) => {
          return actions.order.create({
            intent: "CAPTURE",
            purchase_units: [
              {
                description: "try-fashion-checkout",
                amount: {
                  currency_code: "USD",
                  value: Payprice,
                },
              },
            ],
          });
        },
        onApprove: async (data, actions) => {
        
          const response = await actions.order.capture();
          console.log(response);
          
          console.log(`id: ${response.payer.payer_id}`)
          try{
            _addTransaction({
              variables: {
                tx_ref: response.payer.payer_id.toString(),
                transaction_id: response.id.toString(),
                amount: parseInt(Payprice),
                orders: _orders,
              },
            })
            .then(() => {
              _orders.forEach((order, index) => {
                console.log("Order id", order);
                _setPaidTrue({
                  variables: {
                    id: order.toString(),
                  }
                })            
                  .then((res) => console.log(res.data.updateOrder))
                  .then(() => message.success(`Cart item ${index} paid for`))
                  .then(() => router.reload());
              });
            })
            .catch((err) => message.error("Failed"));
          }catch(err){
            console.log(err)
          }
          if (response.status == "COMPLETED") {
            console.log(response.status)
          } else {
            message.error("Transaction failed");
          }
        },
        onError: (err) => {
          console.log(err);
        },
      })
      .render(paypal.current);
  }

  const handleReset = () => {};

  return basket.length > 0 ? (
    <div>
      <Header />
      <Modal visible={modalVisible} onCancel={handleCancelModal} footer={null}>
        <h2>Checkout</h2>
        <Tabs defaultActiveKey="1">
          <TabPane key="1" tab="Cart">
            {basket.map((order) => (
              <CartItem key={order.id} order={order} />
            ))}
            <Divider orientation="right">Total</Divider>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <p>Total</p>
              <p style={{fontWeight:'bold'}}>KES {getTotal()}</p>
            </div>
          </TabPane>
          <TabPane key="2" tab="Address & Payment">
            <h3 style={{ borderBottom: "2px solid #212222", color: "#d2b48c" }}>
              Billing Details
            </h3>
            <form>
              <p>Name :</p>
              <input
                value={name}
                onChange={(e) => {
                  setname(e.target.value);
                }}
                className={styles.shipping_input}
                type='text'
                placeholder="Name"
                required
              />
              <input
                value={email}
                onChange={(e) => {
                  setemail(e.target.value);
                }}
                className={styles.shipping_input}
                style={{ marginTop: "10px" }}
                placeholder="Email"
                type="email"
                required
              />
            </form>

            <div>
              <p>City :</p>
              <input
                onChange={(e) => {
                  setcity(e.target.value);
                }}
                value={city}
                className={styles.shipping_input}
                placeholder="City"
                required
              />
            </div>
            <div>
              <p>Address :</p>
              <input
                onChange={(e) => {
                  setaddress(e.target.value);
                }}
                value={address}
                className={styles.shipping_input}
                placeholder="Street,Building,Area"
                required
              />
            </div>
            <div>
              <p>Phone:</p>
              <input
                value={phone}
                onChange={(e) => {
                  setphone(e.target.value);
                }}
                className={styles.shipping_input}
                placeholder="Phone number"
                required
              />
            </div>
            <div
              style={{
                display: "",
                justifyContent: "space-around",
                width: "100%",
              }}
            >
              <button
                style={{
                  padding: "5px 10px",
                  border: "none",
                  fontWeight: "800",
                  margin: "10px 0",
                  backgroundColor: "#d2b48c",
                  width:'100%',
                  borderRadius:'5px',
                }}
                onClick={handleSaveNPay}
              >
                Pay With Mpesa/Card
              </button>
              <div  ref={paypal} onClick={HandlePay} 
              style={{
                padding: "5px 10px",
                border: "none",
                fontWeight: "800",
                margin: "10px 0",
                backgroundColor: "#212222",
                width:'100%',
                color:'#fff',
                borderRadius:'5px',
                textAlign:'center'
              }}>Pay with PayPal</div>
              
              <button
              style={{
                padding: "5px 10px",
                border: "1px solid red",
                fontWeight: "800",
                margin: "10px 0",
                backgroundColor: "#fff",
                width:'100%',
                color:'#212222',
                borderRadius:'5px',
                textAlign:'center'
              }}
                onClick={handleCancelModal}
              >
                Cancel
              </button>
            </div>
          </TabPane>
        </Tabs>
      </Modal>
      <div>
        <div className={styles.body}>
          <div className={styles.items}>
            <p
              style={{
                width: "100px",
                height: "40px",
                borderBottom: "2px solid #212222",
              }}
            >
              My Cart
            </p>
            <p onClick={Res}>Click me</p>
            {basket.map((order) => (
              <CartItem key={order.id} order={order} />
            ))}
          </div>
          <div className={styles.checkout}>
            <h3 style={{ borderBottom: "2px solid #212222", color: "#d2b48c" }}>
              Billing Summary
            </h3>
            <div
              style={{
                padding: "10px",
                margin: "10px 0",
                lineHeight: "30px",
                fontSize: "14px",
              }}
            >
              <p>Sub-Total: {getTotal()}</p>

              <p style={{ fontWeight: "700" }}>Total: {getTotal()}</p>
            </div>
            <button
              className={styles.checkout_button}
              onClick={() => {
                setModalVisible(true);
              }}
            >
              {"Proceed to checkout"}
            </button>
            <Crypto price={Payprice} basket={basket} />
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifycontent: "center",
      }}
    >
      <Header />
      <div style={{ margin: "10px auto", lineHeight: "30px" }}>
        <p>Your basket seems to be empty</p>
        <p
          style={{
            padding: "10px",
            backgroundColor: "#d2b48c",
            fontWeight: "800",
            cursor: "pointer",
            textAlign: "center",
          }}
          onClick={() => {
            router.push("/filter");
          }}
        >
          Continue Shopping here
        </p>
        
      </div>
    </div>
  );
}

function Res(){
  const url = "https://cuteprofit.com/account/api/njia";
  const api = "49367f20dbbcd22c5b151cbbf02419af";


  const name = 'sam'
  const email = 'sam@gmail.com'
  const mobile = '123'
  const budget = '3000'
  const transid = 'heck123'

  var xhr = new XMLHttpRequest();
  xhr.open("POST", url);
  
  xhr.setRequestHeader("Content-Type", "application/json");
  
  xhr.onreadystatechange = function () {
     if (xhr.readyState === 4) {
        console.log(xhr.status);
        console.log(xhr.responseText);
     }};
  
  var [data] = `{"fname":${name},"email":${email},"mobile":${mobile},"budget":${budget},"transid":${transid},"firmid":${api}}`;
  console.log(data);
  xhr.send(data);
}

