import "../styles/globals.css";
import "antd/dist/antd.css";
import Script from "next/script";
import WhatsAppIcon from '@mui/icons-material/WhatsApp';

import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
} from "@apollo/client";

const client = new ApolloClient({
  //  uri: "http://127.0.0.1:4000/graphql",
  uri: "https://try-fashions.herokuapp.com/graphql",
  cache: new InMemoryCache(),
});

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <Script src="https://www.paypal.com/sdk/js?client-id=Ac_M2cxYO1tz3Fy8eabJPN1A5yrpH1LCGYdZ9w78BMvRl2fBI_RMIFu5nEqkvP0fWiVKpw4rQwJMxwc6&currency=USD" />
      <Component {...pageProps} />
      <Social />
    </ApolloProvider>
  );
}

export default MyApp;

const Social =()=>{
  return(

    <button 
    onClick={()=>
      window.open('https://web.whatsapp.com/','_blank') 
    }
    style={{display:'flex',
                    width:'',
                    borderRadius:99,
                    outline:'none',
                    border:'none',
                    padding:10,
                    position:'fixed',
                    bottom:15,
                    right:15,
                    zIndex:99,
                    cursor:'pointer',
                    backgroundColor:'#128C7E',
                    boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)'
                  }}>
      <WhatsAppIcon />
      <p style={{margin:0}}>Contact Us</p>
    </button>
  )
  
}