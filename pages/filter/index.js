/* eslint-disable @next/next/no-img-element */
import React, { useState,useEffect } from "react";
import styles from "../../styles/Filter.module.css";
import { useRouter } from "next/router";
import Header from "../../components/Header.js";
import { SpinnerRoundFilled } from "spinners-react";
import ReactPaginate from 'react-paginate-next';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';

import { useQuery,useLazyQuery } from "@apollo/client";
import { GET_CATEGORIES, GET_PRODUCTS } from "./../api/requests.js";

import { Select } from "antd";

const { Option } = Select;

export default function Filter() {
  const {
    data: pcData,
    loading: pcLoading,
    error: pcError,
  } = useQuery(GET_PRODUCTS);

  const [keyword, setKeyword] = useState("");
  
  const {
    data: cData,
    loading: cLoading,
    error: cError,
  } = useQuery(GET_CATEGORIES);

  const getDetails=async()=>{
    const data = await cData;
    console.log(data)
    try{
      console.log(data)
      setNumber(pcData.getProducts.length)
    }catch(err){
      console.log(err)
    }
  };
  
  const [pageNumber, setPageNumber]=useState(0)
  const propertyPerPage= 10;
  const pagesVisted = pageNumber * propertyPerPage;
  const [number,setNumber]=useState('')
  const pageCount = Math.ceil(number/propertyPerPage)

  useEffect(()=>{
    
    getDetails();
  },[])

  
  
  const handlePageClick = ({selected})=>{
    setPageNumber(selected)
}
  const [subCategory, setSubCategory] = useState(null);
  const [sortOrder, setSortOrder] = useState(null);

  if (pcLoading)
    return (
      <div
        style={{
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          margin:"25vh 0"
        }}
      >
        <SpinnerRoundFilled size={100} speed={50} color={"#d2b48c"} />
        <p
          style={{
            marginTop: "10",
            fontWeight: "600",
            fontSize: "2rem",
            display: "flex",
          }}
        >
          Try<p style={{ margin: "0", color: "#d2b48c" }}>Fashion</p>
        </p>
      </div>
    );
  if (pcError) return <p>Error loading data</p>;
  
  return (
    <div>
      <Header getQuery={(val) => setKeyword(val)} />
      <div style={{ display: "flex", margin: "0 10px" }}>
        <Select
          value={subCategory}
          onChange={(val) => setSubCategory(val)}
          style={{ width: 120 }}
        >
          <Option value={null}>Category</Option>
          {cLoading && <p>Loading...</p>}
          {cData?.getCategories.map((el, inx) => (
            <Option key={inx} value={el.name}>
              {el.name.charAt(0).toUpperCase() +
                el.name.slice(1, el.name.length)}
            </Option>
          ))}
        </Select>
        <Select
          value={sortOrder}
          onChange={(val) => setSortOrder(val)}
          style={{ width: 120, marginLeft: "10px" }}
        >
          <Option value={null}>Sort</Option>
          <Select.Option value="a-z">Alphabetically A-Z</Select.Option>
          <Option value="z-a">Alphabetically Z-A</Option>
          <Option value="lowest-price-first">Price low to high</Option>
          <Option value="highest-price-first">Price high to low</Option>
        </Select>
      </div>

      <div
        style={{
          margin: "10px 10px",
          display: "flex",
          alignItems: "baseline",
          backgroundColor: "#d2b48c",
          padding: "10px",
          width: "100%",
        }}
      >
        <span
          style={{ fontWeight: "800px", fontSize: "1rem", fontWeight: "bold" }}
        >
          {subCategory !== null
            ? subCategory.charAt(0).toUpperCase() +
              subCategory.slice(1, subCategory.length)
            : "All products"}
        </span>
        <span
          style={{
            fontSize: "12px",
            fontWeight: "bold",
            padding: "0 10px",
            color: "grey",
          }}
        >
          {
            pcData.getProducts
              .filter((product) =>
                product.name.toLowerCase().includes(keyword.toLowerCase())
              )
              .filter((product) => {
                return subCategory != null
                  ? product.subCategory == subCategory
                  : product;
              })
              .sort((a, b) => {
                if (sortOrder == "highest-price-first") {
                  return b.price - a.price;
                } else if (sortOrder == "lowest-price-first") {
                  return a.price - b.price;
                }
              }).length
          }{" "}
          products
        </span>
      </div>

      <div className={styles.body}>
        <div className={styles.product_section}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              flexWrap: "wrap",
              marginTop: "10px",
            }}
          >
            {pcData.getProducts
              .filter(
                (product) =>
                  product.name.toLowerCase().includes(keyword.toLowerCase()) ||
                  product.name.toLowerCase().includes(val.toLowerCase())
              )
              .filter((product) => {
                return subCategory != null
                  ? product.subCategory == subCategory
                  : product;
              })
              .sort((a, b) => {
                if (sortOrder == "highest-price-first") {
                  return b.price - a.price;
                } else if (sortOrder == "lowest-price-first") {
                  return a.price - b.price;
                }
              }).slice(pagesVisted, pagesVisted + propertyPerPage)
              .map((product) => {
                return <Product key={product.id} product={product} />;
              })}
          </div>
        </div>
        <ReactPaginate 
            breakLabel="..."
            nextLabel=<NavigateNextIcon style={{fontSize:"1rem",border:"1px solid #d2b48c",borderRadius:"99px",marginLeft:"-10px",marginTop:"-2px"}}/>
            onPageChange={handlePageClick}
            pageRangeDisplayed={10}
            pageCount={pageCount}
            previousLabel=<NavigateBeforeIcon style={{fontSize:"1rem",border:"1px solid #d2b48c",borderRadius:"99px",marginRight:"-10px",marginTop:"-2px"}}/>
            renderOnZeroPageCount={null}
            containerClassName={styles.paginationbtns}
            previousLinkClassName={styles.previousbtns}
            nextLinkClassName={styles.nextbtns}
            disabledClassName={styles.paginationDisabled}
            activeClassName={styles.paginationActive}
          />
      </div>
    </div>
  );
}

const Product = ({ product }) => {
  const router = useRouter();

  return (
    <div
      onClick={() => {
        router.push(`/product/${product.id}`);
      }}
      style={{
        width: "200px",
        fontSize: "12px",
        margin: "10px",
        position: "relative",
        cursor:'pointer',
        borderRadius:'5px',
        boxShadow:'2.3px 2px 6.8px rgba(0, 0, 0, 0.073),7.8px 6.7px 23px rgba(0, 0, 0, 0.107),35px 30px 103px rgba(0, 0, 0, 0.18)'
      }}
    >
      <img
        src={product.image1}
        style={{ width: "200px", height: "200px",objectFit:'cover' }}
        alt="image"
      />
      <div
      style={{padding:10,}}
      >
        <p style={{ fontWeight: "600", color: "grey" }}>{product.name}</p>
        <p style={{ fontWeight: "600" }}>Ksh. {product.price}</p>
      </div>
    </div>
  );
};
