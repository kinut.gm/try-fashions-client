import React, { useState } from "react";
import { useRouter } from "next/router";
import styles from "../styles/Cart.module.css";
import { Select } from "antd";

import { REMOVE_FROM_BASKET } from "../pages/api/requests.js";
import { useMutation } from "@apollo/client";

const { Option } = Select;

export default function CartItem({ order }) {
  const router = useRouter();
  const adjustedprice = order.product.price * order.quantity;

  const [_removeItem] = useMutation(REMOVE_FROM_BASKET);

  const handleRemove = () => {
    _removeItem({
      variables: {
        id: order.id,
      },
    })
      .then((res) => {
        alert("Product removed from cart");
        router.reload();
      })
      .catch((err) => alert(err.message));
  };

  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        fontSize: "12px",
        margin: "10px",
        position: "relative",
      }}
    >
      <img
        onClick={() => {
          router.push("product");
        }}
        src={order.product.image1}
        style={{ width: "100px", height: "100px" }}
        alt="image"
      />
      <div style={{ margin: "0 10px", flex: 1 }}>
        <p style={{ fontWeight: "600", color: "grey" }}>{order.product.name}</p>
        <p style={{ fontWeight: "600" }}>Ksh {adjustedprice}</p>
        <div className={styles.value_container}>
          <Select value={order.quantity} disabled></Select>
          <button
            style={{
              border: "none",
              padding: "10px",
              margin: "0 10px",
              color: "red",
              fontWeight: "700",
            }}
            onClick={handleRemove}
          >
            Remove
          </button>
        </div>
      </div>
    </div>
  );
}
