import React, { useState } from 'react'
import { message, Modal } from "antd";
import {useRouter} from 'next/router';
import styles from "../styles/Cart.module.css";
import {  useMutation } from "@apollo/client";
import {  ADD_TRANSACTION } from "../pages/api/requests.js";


export default function Crypto({price,basket}){
  const router = useRouter();
    const [data,setData]=useState('');

    const [_addTransaction] = useMutation(ADD_TRANSACTION);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const handlePay = async()=>{
        
        const res = await fetch('http://localhost:5000/crytpo-payment/us-central1/createCharge');
        const response = await res.json();

        console.log(response.id)
        setIsModalVisible(true);

        let _orders = [];

        basket.forEach((order) => _orders.push(order.id));
        console.log(_orders)
        try{
          _addTransaction({
            variables: {
              tx_ref: Date.now(),
              transaction_id: response.id.toString(),
              amount: price,
              orders: _orders,
            },
          })
          .then(() => message.success(`Cart item ${index} paid for`))
          .then(() => router.reload());
        }catch(err){
          message.error('error-contact support')
        }
        setData(response.hosted_url);
    }

    const handleOk = () => {
        setIsModalVisible(false);
      };
    
      const handleCancel = () => {
        setIsModalVisible(false);
      };

    return(
        <div>
        
          <button onClick={handlePay} className={styles.checkout_button}>
              Pay with CryptoCurrency
          </button>

          <Modal title="CryptoCurrency" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <button className={styles.checkout_button}>
              <a href={data}  target="_blank"
                rel="noopener noreferrer">Click to Pay USD {price} </a>
            </button>    
          
          </Modal>

        </div>
    )
}