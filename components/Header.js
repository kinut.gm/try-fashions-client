import React, { useEffect, useState } from "react";
import styles from "../styles/Home.module.css";
import Link from "next/link";
import { useRouter } from "next/router";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import SearchIcon from "@mui/icons-material/Search";
import MenuIcon from "@mui/icons-material/Menu";
import HomeIcon from "@mui/icons-material/Home";
import Close from '@mui/icons-material/Close';
import { Tabs } from 'antd';

import { useQuery } from "@apollo/client";
import { GET_CATEGORIES,GET_PRODUCTS } from "../pages/api/requests.js";

export default function Header({ getQuery }) {
  const router = useRouter();
  const [showmenu, setShowmenu] = useState("");
  const [showsearch, setShowSearch] = useState("");
  
  const HandleMenu = () => {
    setShowmenu(!showmenu);
  };
  const HandleSearch = () => {
    setShowSearch(!showsearch);
  };

  const getKeyword = (word) => {
    getQuery(word);
  };

  return (
    <div
      style={{
        backgroundColor: "#fff",
        padding: "10px",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        position: "sticky",
        zIndex: "1",
        top: "0px",
      }}
    >
      <p
        onClick={() => {
          router.push("/");
        }}
        style={{
          margin: "0",
          fontWeight: "600",
          fontSize: "1.2rem",
          display: "flex",
          cursor: "pointer",
        }}
      >
        Try<p style={{ margin: "0", color: "#d2b48c" }}>Fashion</p>
      </p>
      <div style={{ color: "", display: "flex", alignItems: "center" }}>
        <HomeIcon
          onClick={() => {
            router.push("/");
          }}
          style={{ marginRight: "10px" }}
        />
        {showsearch ? (
          <div>
            <SearchIcon
              onClick={HandleSearch}
              style={{ marginRight: "10px", marginTop: "10px" }}
            />
            <Searchbar onClick={HandleSearch} getKeyword={getKeyword} />
          </div>
        ) : (
          <SearchIcon onClick={HandleSearch} style={{ marginRight: "10px" }} />
        )}
        <ShoppingCartIcon
          onClick={() => {
            window.location = "/cart";
          }}
          style={{ marginRight: "10px" }}
        />
        <Link href="/account" passHref>
          <AccountBoxIcon
            onClick={() => {
              router.push("account");
            }}
            style={{ marginRight: "10px" }}
          />
        </Link>
        {showmenu ? (
          <div>
            <Close onClick={HandleMenu} style={{ marginTop: "10px" }} />
            <Menubar />
          </div>
        ) : (
          <MenuIcon style={{ marginTop: "0px" }} onClick={HandleMenu} />
        )}
      </div>
    </div>
  );
}

const Menubar = () => {
  const {
    data: cData,
    loading: cLoading,
    error: cError,
  } = useQuery(GET_CATEGORIES);

  if (cLoading) return <p>...</p>;
  if (cError) return <p>Error...</p>;

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        fontFamily: "Poppins-Regular",
        fontSize: "1rem",
        position: "absolute",
        top: "60px",
        right: "-10px",
        width: "300px",
        height: "100vh",
        background: "white",
        zIndex: "9",
      }}
    >
      <h2 style={{ padding: "12px ",backgroundColor:"#d2b48c",fontWeight:"bold",margin:0 }}>Categories</h2>
      <Women cData={cData} />
    </div>
  );
};

const Women = ({ cData }) => {
  const { TabPane } = Tabs;

  const items = [
    { name: "NEW IN", link: "" },
    { name: "ALL CLOTHING", link: "" },
  ];

  const {
    data: pcData,
    loading: pcLoading,
    error: pcError,
  } = useQuery(GET_PRODUCTS);

  return (
    <div
      style={{
        padding: "0 10px",
        width: "100%",
        height: "100%",
        backgroundColor: "#fff",
        margin: "0",
        display: "flex",
        flexDirection: "column",
        overflowY:"scroll"
      }}
    >
    <Tabs type="card">
    <TabPane tab="All" key="1">
    <div style={{
      padding: "10px 0",
      margin: "0",
      display: "flex",
      flexDirection: "column",
    }}>
    {items.map((item) => {
      return (
        <span
          onClick={() =>
            (window.location = `/category/${item.name.toLowerCase()}`)
          }
          key={item.id}
          style={{
            padding: "10px 0",
            borderBottom: "1px solid grey",
          }}
        >
          {item.name}
        </span>
      );
    })}
    </div>
    </TabPane>
    <TabPane tab="Women" key="2">
    <div style={{
      padding: "10px 0",
      margin: "0",
      display: "flex",
      flexDirection: "column",
    }}>
    <p onClick={() =>
      (window.location = `/category/ladies`)
    }
    style={{
      padding: "10px 0",
      borderBottom: "1px solid grey",
    }} 
    >All</p>
      {pcData?.getProducts
        .filter(
          (product) =>
          product.category.toLowerCase().includes("ladies")
        )
        .map((product) => {
          return (

            <p key={product.id} 
            onClick={() =>
              (window.location = `/category/${product.subCategory.toLowerCase()}`)
            }
            style={{
              padding: "10px 0",
              borderBottom: "1px solid grey",
              textTransform: "lowercase",
            }} >{product.subCategory}</p>
          )
        })}
    </div>
    </TabPane>
    <TabPane tab="Men" key="3">
    <div style={{
      padding: "10px 0",
      margin: "0",
      display: "flex",
      flexDirection: "column",
    }}>
    <p onClick={() =>
      (window.location = `/category/men`)
    }
    style={{
      padding: "10px 0",
      borderBottom: "1px solid grey",
    }} 
    >All</p>
    {pcData?.getProducts
      .filter(
        (product) =>
        product.category.toLowerCase().includes("men")
      )
      .map((product) => {
        return (

          <p key={product.id} 
          onClick={() =>
            (window.location = `/category/${product.subCategory.toLowerCase()}`)
          }
          style={{
            padding: "10px 0",
            borderBottom: "1px solid grey",
            textTransform: "lowercase",
          }} >{product.subCategory}</p>
        )
      })}
    </div>
    </TabPane>
    <TabPane tab="Kids" key="4">
    <p onClick={() =>
      (window.location = `/category/kids`)
    }
    style={{
      padding: "10px 0",
      borderBottom: "1px solid grey",
    }} 
    >All</p>
    {pcData?.getProducts
      .filter(
        (product) =>
        product.category.toLowerCase().includes("kids")
      )
      .map((product) => {
        return (

          <p key={product.id} 
          onClick={() =>
            (window.location = `/category/${product.subCategory.toLowerCase()}`)
          }
          style={{
            padding: "10px 0",
            borderBottom: "1px solid grey",
            textTransform: "lowercase",
          }} >{product.subCategory}</p>
        )
      })}
    </TabPane>
  </Tabs> 
      
    </div>
  );
};


const Searchbar = ({ getKeyword, onClick }) => {
  const [keyword, setKeyword] = useState("");
  return (
    <div
    className={styles.fadeInUp}
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "",
        fontFamily: "Poppins-Regular",
        fontSize: "1rem",
        position: "absolute",
        top: "60px",
        height:"100vh",
        transition:"ease-in-out",
        transitionDelay:"2s",
        right: "-10px",
        width: "300px",
        zIndex: "999",
        backgroundColor: "#E5E5E5",
        opacity: "0.9",
      }}
    >
    <div style={{margin:"5px 10px"}}>
      <Close onClick={onClick}  style={{float:"right"}}/>
    </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          width: "300px",
        }}
      >
        <input
          value={keyword}
          onChange={(e) => setKeyword(e.target.value)}
          placeholder="what are you looking for?"
          style={{
            fontSize: "0.9rem",
            outline: "none",
            padding: "5px",
            border: "none",
            height: "100%",
            width: "80%",
            marginTop: "10px",
            overflowWrap: "break-word",
          }}
        />
        <button
          onClick={(e) => {
            getKeyword(keyword);
          }}
          style={{
            paddingTop: "5px",
            border: "none",
            width: "40px",
            height: "40px",
            backgroundColor: "#d2b48c",
            marginTop: "9px",
          }}
        >
          <SearchIcon />
        </button>
        
      </div>
      <div style={{ padding: "10px" }}>
        <span style={{ textDecoration: "underline" }}>Recent searches</span>
        <div style={{ display: "flex", flexDirection: "column" }}>
          
        </div>
      </div>
    </div>
  );
};

